service: TesteMurillo

provider:
  name: aws
  runtime: nodejs8.10
  stage: ${opt:stage, 'dev'}
  region: ${self:provider.environment.ENV_AWS_REGION}
  accountId: ${self:provider.environment.ENV_AWS_ACCOUNT_ID}

  stackName: ${self:service}-${self:provider.stage}
  apiName: ${self:provider.environment.AWS_APIGATEWAY_NAME}
  apiKeys:
    - ${self:service}
  deploymentBucket:
    name: serverlessdeploys-${self:provider.environment.ENV_AWS_REGION}
  deploymentPrefix: ${self:provider.environment.PROJECT}

  iamRoleStatements:
    - Effect: Allow
      Action:
        - dynamodb:Query
        - dynamodb:Scan
        - dynamodb:GetItem
        - dynamodb:PutItem
        - dynamodb:UpdateItem
        - dynamodb:DeleteItem
      Resource: "arn:aws:dynamodb:${self:provider.region}:*:table/TesteMurilloOrders"

  environment:
    ${file(./.env.${self:provider.stage}.yml)}

functions:
  dynamodb_stream:
    handler: src/dynamodb/stream.handler
    events:
      #      - stream: arn:aws:dynamodb:${self:provider.region}:${self:provider.environment.ENV_AWS_ACCOUNT_ID}:table/TesteMurilloOrders/stream/${self:provider.environment.AWS_DYNAMODB_STREAM}
      - stream:
          type: dynamodb
          batchSize: 1
          arn:
            Fn::GetAtt:
              - DynamoDBTableOrders
              - StreamArn
  get-order:
    handler: src/order/http.get
    events:
      - http:
          path: /orders/{orderId}
          method: get
          cors: true
          private: false
  post-orders:
    handler: src/order/http.post
    events:
      - http:
          path: /orders
          method: post
          cors: true
          private: false
  consumerOrdersToProcess:
    handler: src/queue/consumer.handler
    events:
      - sqs: arn:aws:sqs:${self:provider.region}:${self:provider.environment.ENV_AWS_ACCOUNT_ID}:TesteMurilloOrdersToProcess

resources:
  Resources:
    DynamoDBTableOrders:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: TesteMurilloOrders
        AttributeDefinitions:
          - AttributeName: id
            AttributeType: S
        KeySchema:
          - AttributeName: id
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
        StreamSpecification:
          StreamViewType: NEW_AND_OLD_IMAGES
    QueueOrdersToProcess:
      Type: AWS::SQS::Queue
      Properties:
        QueueName: TesteMurilloOrdersToProcess

plugins:
  - serverless-dynamodb-local
  - serverless-offline-dynamodb-streams
  - serverless-offline-sqs
  - serverless-offline

custom:
  stages:
    - dev
    - staging
    - prod
  serverless-offline:
    host: 0.0.0.0
  dynamodb:
    stages:
      - dev
    start:
      host: ${self:provider.environment.AWS_DYNAMODB_HOST}
      port: ${self:provider.environment.AWS_DYNAMODB_PORT}
      noStart: true
      migrate: true
      seed: true
    seed:
      domain:
        sources:
          - table: TesteMurilloOrders
            sources: [./resources/dynamoDb/seed/Orders.json]
  serverless-offline-dynamodb-streams:
    endpoint: ${self:provider.environment.AWS_DYNAMODB_ENDPOINT}
    region: ${self:provider.region}
    accessKeyId: ${self:provider.environment.ENV_AWS_ACCESS_KEY_ID}
    secretAccessKey: ${self:provider.environment.ENV_AWS_SECRET_ACCESS_KEY}
  serverless-offline-sqs:
    endpoint: ${self:provider.environment.AWS_SQS_ENDPOINT}
    region: ${self:provider.region}
    accessKeyId: ${self:provider.environment.ENV_AWS_ACCESS_KEY_ID}
    secretAccessKey: ${self:provider.environment.ENV_AWS_SECRET_ACCESS_KEY}
