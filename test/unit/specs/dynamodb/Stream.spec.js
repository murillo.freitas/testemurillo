const stream = require('../../../../src/dynamodb/stream');

const AWS = require('aws-sdk');
jest.mock('aws-sdk');

const callback = _ => _;

describe('dynamodb/stream.js', () => {
  let result;
  let mock;
  beforeEach(() => {
    mock = jest.fn(() => {
      result = Promise.resolve({});
      return {
        promise: callback,
      };
    });
    AWS.SQS = jest.fn().mockImplementation(() => {
      return {
        sendMessage: mock,
      };
    });
  });

  it('should send a message to queue when the dynamo event is INSERT', async (done) => {
    let event = {
      Records: [{
        eventName: 'INSERT',
        dynamodb: {
          NewImage: AWS.DynamoDB.Converter.marshall({ id: 123 }),
        },
      }],
    };

    stream.handler(event, null, callback);

    return result
      .then(data => {
        expect(data).toEqual({});
        done();
      });
  });

  it('should send a message to queue when the dynamo event is MODIFY', async (done) => {
    let event = {
      Records: [{
        eventName: 'MODIFY',
        dynamodb: {
          NewImage: AWS.DynamoDB.Converter.marshall({ id: 123 }),
        },
      }],
    };

    stream.handler(event, null, callback);

    return result
      .then(data => {
        expect(data).toEqual({});
        done();
      });
  });

  it('should not call SQS.sendMessage to send a message to queue when the dynamo event is not INSERT or MODIFY', async (done) => {
    const randomStringGenerator = require('randomstring').generate;
    for (let i = 0; i < 1; i++) {
      let randomString = randomStringGenerator();
      while (randomString === 'INSERT' || randomString === 'MODIFY') {
        randomString = randomStringGenerator();
      }

      let event = {
        Records: [{
          eventName: randomString,
          dynamodb: {
            NewImage: '',
          },
        }],
      };

      await stream.handler(event, null, callback);
      expect(mock).not.toHaveBeenCalled();
    }
    done();
  });
});