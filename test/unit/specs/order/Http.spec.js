const orderHttp = require('../../../../src/order/http');

const orderService = require('../../../../src/services/orderService');
const invoiceService = require('../../../../src/services/invoiceService');
const transferService = require('../../../../src/services/transferService');
jest.mock('../../../../src/services/orderService');
jest.mock('../../../../src/services/invoiceService');
jest.mock('../../../../src/services/transferService');

describe('order/http.js', () => {
  let event;
  let mockOrderObject;
  let mockInvoiceObject;
  let mockTransferObject;
  beforeEach(() => {
    mockOrderObject = {
      id: '1',
      status: 'Created',
      price: 26700,
      cartID: '2',
      partnerID: '3',
      customerID: '4',
      invoiceID: null,
      transferID: null,
    };
    mockInvoiceObject = {
      id: '5',
      order: '1',
    };
    mockTransferObject = {
      id: '6',
      order: '1',
    };
    orderService.get = jest.fn(() => mockOrderObject);
    invoiceService.get = jest.fn(() => mockInvoiceObject);
    transferService.get = jest.fn(() => mockTransferObject);

    event = {
      pathParameters: {
        orderId: 123,
      },
    };
  });

  describe('test get', () => {
    it('should return a successfully response with no extra parameters', async () => {
      const result = await orderHttp.get(event);
      expect(result)
        .toEqual({
          body: JSON.stringify(mockOrderObject),
          headers: { 'Access-Control-Allow-Origin': '*' },
          statusCode: 200,
        });
    });
    it('should return a successfully response with invoice information', async () => {
      mockOrderObject.invoiceID = '5';
      orderService.get = jest.fn(() => mockOrderObject);

      Object.assign(mockOrderObject, {
        invoice: mockInvoiceObject,
      });
      const result = await orderHttp.get(event);
      expect(result)
        .toEqual({
          body: JSON.stringify(mockOrderObject),
          headers: { 'Access-Control-Allow-Origin': '*' },
          statusCode: 200,
        });
    });
    it('should return a successfully response with transfer information', async () => {
      mockOrderObject.transferID = '6';
      orderService.get = jest.fn(() => mockOrderObject);

      Object.assign(mockOrderObject, {
        transfer: mockTransferObject,
      });
      const result = await orderHttp.get(event);
      expect(result)
        .toEqual({
          body: JSON.stringify(mockOrderObject),
          headers: { 'Access-Control-Allow-Origin': '*' },
          statusCode: 200,
        });
    });
    it('should return error response', async () => {
      orderService.get = jest.fn(() => Promise.reject({ code: 999 }));
      const result = await orderHttp.get(event);
      expect(result)
        .toEqual({
          body: JSON.stringify({ code: 999 }),
          headers: { 'Access-Control-Allow-Origin': '*' },
          statusCode: 999,
        });
    });
  });

  describe('test post', () => {
    it('should return a successfully response', async () => {
      const eventPost = {
        body: JSON.stringify({
          'status': 'Created',
          'price': 26700,
          'cartID': '90CCF5CE-DEC7-D717-71F9-D478628F601F',
          'partnerID': 'F851E323-E4AF-8F66-C95A-32D96A1928E4',
          'customerID': 'ADBF8C19-9348-4B92-194F-C1EF4E6B1E48',
        }),
      };

      orderService.create = jest.fn(() => Promise.resolve({}));

      Object.assign(mockOrderObject, {
        Location: `/orders/${mockOrderObject.id}`,
      });

      const result = await orderHttp.post(eventPost);
      expect(result.statusCode).toEqual(201);
    });
    it('should return a error response', async () => {
      const eventPost = {
        body: JSON.stringify({
          asda: '1',
          status: 'Created',
        }),
      };

      orderService.create = jest.fn(() => Promise.reject({ code: 999 }));

      const result = await orderHttp.post(eventPost);
      expect(result.statusCode).toEqual(undefined);
    });
  });
});