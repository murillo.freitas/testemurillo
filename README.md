# TesteMurillo

This project was built based on serverless framework.

As I have been told the primary cloud you use is Amazon AWS so I decided to build this project using only AWS.
I also have decided use a queue concept using the ampq.

So basically this project uses:

- API Gateway
- DynamoDB
- DynamoDBStream
- Lambda
- SQS

It exposes two API endpoints and consume a queue.

After run the project five ports will be shared between the host and the containers, so you can access it using `http://localhost:{port}`, and two service will start in background

The ports are:
 - 3000 => Order API (This project)
 - 3001 => Invoices Mocked API
 - 3002 => Transfers Mocked API
 - 8000 => DynamoDB Local Instance
 - 9324 => SQS Local Instance
 
At the port 3000 two `rest` routes are available.

```
GET /orders/{orderId}
POST /orders
```
 
These two services started in background are responsible to:

- First job => Listen to all DynamoDB Streams and check if it should be queued in SQS. 
- Second job => Consume the SQS queue and make the invoice/transfer creation and update the order. 

So to summarize:
 1. The API is responsible to get an `order` from DynamoDB
 2. The API is responsible to create an `order` and save it to DynamoDB
 3. DynamoDB trigger streams in every change made in the database.
 4. The Stream is analysed and forwarded to the queue when it is an `INSERT` or `UPDATE`
 4. The queue is consumed by a task that is responsible to verify what `status` the `order` has and then choose if it should be create (invoice/transfer) and then update the `order` (this action will generate an Stream. See step 3) or if it should be discarded

### Pre-requisites

 - Docker
 - Docker Compose

### Project running

\$ docker-compose up

### OBS

1. When executed in development and seed file will run and populate the DynamoDB Local.
This will trigger the streams and will start all the background jobs and the orders will start to have their status updated until they reach the `Completed` status.

2. This project has an CI/CD enabled to `gitlab` so if you decide to fork to try this on your own gitlab account is important to configure two environments variables in the project with your own key.
 - AWS_ACCESS_KEY_ID
 - AWS_SECRET_ACCESS_KEY
 
3. In development environment I have used the two mocks APIS so before execute the CI/CD on gitlab change the `API_INVOICES_ENDPOINT` and `API_TRANSFERS_ENDPOINT` .env.{env}.yml
