FROM node:8-alpine

RUN npm install -g serverless

WORKDIR /app
COPY ./package.json ./package-lock.* /app/

RUN yarn