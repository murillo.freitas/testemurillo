'use strict';

const headers = { 'Access-Control-Allow-Origin': '*' };

module.exports = {
  toJSON: function (statusCode, responseData = {}, extraHeaders = {}) {
    return {
      statusCode,
      headers: Object.assign(headers, extraHeaders),
      body: JSON.stringify(responseData),
    };
  },
};