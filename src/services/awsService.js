'use strict';

module.exports.getAWS = () => {
  const AWS = require('aws-sdk');

  if (process.env.IS_OFFLINE) {
    AWS.config.update({
      region: process.env.ENV_AWS_REGION,
      endpoint: process.env.AWS_DYNAMODB_ENDPOINT,
      accessKeyId: process.env.ENV_AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.ENV_AWS_SECRET_ACCESS_KEY,
    });
  }

  return AWS;
};