'use strict';

const AWS = require('../services/awsService').getAWS();

const dynamoDBDoc = new AWS.DynamoDB.DocumentClient();
const tableName = 'TesteMurilloOrders';

module.exports = {
  get: function (orderId) {
    return new Promise((resolve, reject) => {
      return dynamoDBDoc.get(
        {
          TableName: tableName,
          Key: { id: orderId },
        },
        (err, data) => {
          if (err) {
            console.log(err);
            return reject({
              code: 500,
              message: 'Error on trying to find the requested resource on database. Try again in a few seconds',
            });
          }

          if (Object.keys(data).length === 0) {
            return reject({
              code: 404,
              message: 'Order not found. Make sure the request is valid',
            });
          }

          return resolve(data.Item);
        },
      );
    });
  },
  create: function (order) {
    return dynamoDBDoc
      .put({
        TableName: tableName,
        Item: order,
      })
      .promise();
  },
  update: function (params) {
    return new Promise((resolve, reject) => {
      return dynamoDBDoc.update(params, (err, data) => {
        if (!err) return resolve(data);

        return reject({
          code: 500,
          message: 'Wasn\'t possible to save you bankslip right now. Try again in a few minutes.',
        });
      });
    });
  },
};