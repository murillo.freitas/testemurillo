'use strict';

const axios = require('axios');
const uuidv1 = require('uuid/v1');

const axiosInstance = axios.create({
  baseURL: process.env.API_TRANSFERS_ENDPOINT,
  headers: { 'Content-type': 'application/json' },
});

module.exports = {
  get: async (transferID) => {
    const { data } = await axiosInstance.get(`/transfers/${transferID}`);
    return data;
  },
  create: async (order) => {

    const invoice = {
      id: uuidv1(),
      order: order.id,
    };

    const { data } = await axiosInstance.post('/transfers', invoice);
    return data;
  },
};