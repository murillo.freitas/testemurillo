'use strict';

const axios = require('axios');
const uuidv1 = require('uuid/v1');

const axiosInstance = axios.create({
  baseURL: process.env.API_INVOICES_ENDPOINT,
  headers: { 'Content-type': 'application/json' },
});

module.exports = {
  get: async (invoiceID) => {
    const { data } = await axiosInstance.get(`/invoices/${invoiceID}`);
    return data;
  },
  create: async (order) => {

    const invoice = {
      id: uuidv1(),
      order: order.id,
    };

    const { data } = await axiosInstance.post('/invoices', invoice);
    return data;
  },
};