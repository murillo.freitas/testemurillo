'use strict';

const invoiceService = require('../services/invoiceService');
const orderService = require('../services/orderService');
const transferService = require('../services/transferService');

const ORDER_STATUS_CHARGED = 'Charged';
const ORDER_STATUS_CREATED = 'Created';

module.exports.handler = async (event, context, callback) => {
  await event.Records.forEach(async (record) => {

    const order = JSON.parse(record.body);

    switch (order.status) {
      case ORDER_STATUS_CHARGED:
        const { id } = await transferService.create(order);
        order.status = 'Completed';
        order.transferID = id;
        break;
      case ORDER_STATUS_CREATED:
        const invoice = await invoiceService.create(order);
        order.status = 'Charged';
        order.invoiceID = invoice.id;
        break;
    }

    await orderService.create(order);

  });

  callback(null, 'CONSUMER QUEUE FINISHED');
};