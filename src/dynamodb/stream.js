'use strict';

const AWS = require('../services/awsService').getAWS();

const STREAM_INSERT = 'INSERT';
const STREAM_UPDATE = 'MODIFY';
const SQS_QUEUE = 'TesteMurilloOrdersToProcess';

module.exports.handler = async (event, context, callback) => {
  const SQS = new AWS.SQS();

  await event.Records.forEach(async (records) => {
    if (
      records.eventName === STREAM_INSERT
      || records.eventName === STREAM_UPDATE
    ) {
      const order = AWS.DynamoDB.Converter.unmarshall(records.dynamodb.NewImage);
      await SQS
        .sendMessage({
          DelaySeconds: 10,
          MessageBody: JSON.stringify(order),
          QueueUrl: `${process.env.AWS_SQS_ENDPOINT}/queue/${SQS_QUEUE}`,
        })
        .promise();
    }
  });

  callback(null, 'DYNAMODB STREAM FINISHED');
};