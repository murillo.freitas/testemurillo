'use strict';
//const uuidv1 = require('uuid/v1');

const validate = require('../models/validator');
const invoiceService = require('../services/invoiceService');
const orderService = require('../services/orderService');
const responseService = require('../services/responseService');
const transferService = require('../services/transferService');

module.exports = {
  get: async (event) => {
    try {
      const { orderId } = event.pathParameters;
      const order = await orderService.get(orderId);

      if (order.invoiceID) {
        order.invoice = await invoiceService.get(order.invoiceID);
      }

      if (order.transferID) {
        order.transfer = await transferService.get(order.transferID);
      }

      return responseService.toJSON(200, order);
    } catch (error) {
      return responseService.toJSON(error.code, error);
    }
  },
  post: async (event) => {
    const order = JSON.parse(event.body);
    return validate(order, 'Order')
      .then(async () => {
//      order.id = uuidv1();

        order.id = order.cartID;
        order.invoiceID = null;
        order.transferID = null;
        await orderService.create(order);

        return responseService.toJSON(201, order, { Location: `/orders/${order.id}` });
      })
      .catch((error) => responseService.toJSON(error.code, error));
  },
};
