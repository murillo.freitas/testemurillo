'use strict';

const jsonValidator = require('jsonschema');

module.exports = function validate(object, model) {
  return new Promise((resolve, reject) => {
    let modelSchema = model;
    if (typeof model === 'string') {
      modelSchema = require(`./${model}.json`);
    }

    const validation = jsonValidator.validate(object, modelSchema);
    if (validation.errors.length > 0)
      return reject(validation.errors);
    return resolve();
  });
};